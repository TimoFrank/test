package de.franks0ft.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import de.franks0ft.Application;
import de.franks0ft.listener.ClickCallback;
import de.franks0ft.resources.Resources;

public class MainMenuStage extends Stage
{
	private VerticalGroup vg;
	private Label title;
	private TextButton play;
	private TextButton quit;

	public MainMenuStage()
	{
		super(new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));

		// Set Background
		Application.assetManager.load("images/backgrounds/mainmenu.png", Texture.class);
		Application.assetManager.finishLoadingAsset("images/backgrounds/mainmenu.png");
		Texture background = Application.assetManager.get("images/backgrounds/mainmenu.png");
		
		Image backgroundImage = new Image(background);
		addActor(backgroundImage);
		
		vg = new VerticalGroup();
		vg.setFillParent(true);
		vg.pad(30);

		// Load font
		Application.assetManager.load("fonts/blocko.fnt", BitmapFont.class);
		Application.assetManager.finishLoadingAsset("fonts/blocko.fnt");
		BitmapFont font = Application.assetManager.get("fonts/blocko.fnt");

		// Set labelstyle
		LabelStyle labelStyle = new LabelStyle(font, new Color(Color.WHITE));
		// Set Title
		title = new Label("Stupid Robot", labelStyle);
		title.setFontScale(4.6f);

		// Set playbuttonstyle
		TextButtonStyle playButtonStyle = new TextButtonStyle();
		playButtonStyle.font = font;
		playButtonStyle.fontColor = new Color(Color.GREEN);
		playButtonStyle.overFontColor = new Color(Color.CYAN);
		playButtonStyle.downFontColor = new Color(Color.FOREST);

		// Set Playbutton
		play = new TextButton("Play", playButtonStyle);
		play.padTop(120);

		// Set buttonstyle
		TextButtonStyle quitButtonStyle = new TextButtonStyle();
		quitButtonStyle.font = font;
		quitButtonStyle.fontColor = new Color(Color.RED);
		quitButtonStyle.overFontColor = new Color(Color.ORANGE);
		quitButtonStyle.downFontColor = new Color(Color.SLATE);
		// Set Quitbutton
		quit = new TextButton("Quit", quitButtonStyle);
		quit.padTop(50);

		vg.addActor(title);
		vg.addActor(play);
		vg.addActor(quit);
		addActor(vg);
	}

	public void addOnPlayListener(ClickCallback cc)
	{
		play.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				cc.clicked(event, x, y);
			}
		});
	}

	public void addOnQuitListener(ClickCallback cc)
	{
		quit.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				cc.clicked(event, x, y);
			}
		});
	}
}
