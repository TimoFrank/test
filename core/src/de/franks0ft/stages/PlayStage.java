package de.franks0ft.stages;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import de.franks0ft.Application;
import de.franks0ft.maps.MapActor;
import de.franks0ft.maps.MapLoader;

public class PlayStage extends Stage
{
	Texture tex;
	Table table;
	
	public PlayStage()
	{
		super(new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		
		Application.assetManager.load("badlogic.jpg", Texture.class);
		Application.assetManager.finishLoadingAsset("badlogic.jpg");
		tex = Application.assetManager.get("badlogic.jpg");
		
		table = new Table();
	    table.setFillParent(true);
	    
		MapActor map = new MapActor(getViewport().getCamera(), "test");
		map.setHeight(Gdx.graphics.getHeight());
		map.setWidth(Gdx.graphics.getWidth());
	    
	    table.add(map);
//	    table.add(new Image(tex));
	    
	    addActor(table);
	}
	
//	@Override
//	public void draw()
//	{
//		for (Actor actor : getActors())
//		{
//			actor.draw(getBatch(), 0f);
//			System.out.println(actor.localToStageCoordinates(new Vector2(0, 0)));
//		}
//		
////		getBatch().begin();
////		getBatch().draw(tex, 0, 0);
////		getBatch().end();
//	}
}
