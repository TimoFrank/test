package de.franks0ft.maps;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class MapLoader
{
	private static final TmxMapLoader tmxMapLoader = new TmxMapLoader();
	
	public static TiledMap loadMap(String name)
	{
		TiledMap map = tmxMapLoader.load("maps/" + name + ".tmx");
		
		return tmxMapLoader.load("maps/" + name + ".tmx");
	}
}
