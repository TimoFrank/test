package de.franks0ft.maps;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MapActor extends Actor
{
	public TiledMap map;
	public TiledMapRenderer tiledMapRenderer;
	Camera cam;
	
	public MapActor(Camera camera, String mapName)
	{
		this.cam = camera;
		map = new TmxMapLoader().load("maps/" + mapName + ".tmx");	
		tiledMapRenderer = new OrthogonalTiledMapRenderer(map);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		tiledMapRenderer.setView(cam.combined, getX(), getY(), getWidth(), getHeight());
		tiledMapRenderer.render();
	}	
}
