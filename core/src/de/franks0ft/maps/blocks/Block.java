package de.franks0ft.maps.blocks;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

public interface Block
{
	public Image getImage();
	public Boolean isWall();
}
