package de.franks0ft.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.physics.box2d.World;

public class Resources
{
	public static Resources instance = null;
	
	public static final float MTP = 12f;
	public static final float PTM = 1 / MTP;
	public static final int VELOCITY_ITERATIONS = 10;
	public static final int POSITION_ITERATIONS = 3;
	
//	private AssetManager am;
	private World world;
	
	protected Resources() 
	{
//		am = new AssetManager();
	}
	
	public static Resources I()
	{
		if (instance == null)
		{
			return new Resources();
		}
		return instance;
	}
	
//	public AssetManager getAssetManager()
//	{
//		return am;
//	}
}
