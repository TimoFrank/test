package de.franks0ft.listener;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

public interface ClickCallback
{
	public void clicked(InputEvent event, float x, float y);
}
