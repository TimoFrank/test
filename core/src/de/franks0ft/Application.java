package de.franks0ft;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.franks0ft.resources.Resources;
import de.franks0ft.stages.MainMenuStage;
import de.franks0ft.stages.PlayStage;

public class Application extends ApplicationAdapter
{
	public static AssetManager assetManager;
	Stage currentStage;
	MainMenuStage menu;

	@Override
	public void create()
	{
		assetManager = new AssetManager();
		
		menu = new MainMenuStage();
		menu.addOnPlayListener(this::onPlay);
		menu.addOnQuitListener(this::onQuit);
		
		setStage(menu);
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		currentStage.act(Gdx.graphics.getDeltaTime());
		currentStage.draw();
	}

	private void setStage(Stage stage)
	{
		Gdx.input.setInputProcessor(stage);
		currentStage = stage;
	}

	private boolean onPlay(InputEvent event, float x, float y)
	{
		System.out.println("Play");
		setStage(new PlayStage());
		return true;
	}

	private boolean onQuit(InputEvent event, float x, float y)
	{
		System.out.println("Quit");
		Gdx.app.exit();
		return true;
	}
}
